import { Component, OnInit } from '@angular/core';
import { Dish} from "../PojoClasses/Dish";
import { Input } from "@angular/core";

@Component({
  selector: 'app-dish-details',
  templateUrl: './dish-details.component.html',
  styleUrls: ['./dish-details.component.less']
})
export class DishDetailsComponent implements OnInit {

  @Input()
  dish:Dish;

  constructor() { }

  ngOnInit() {
  }

}

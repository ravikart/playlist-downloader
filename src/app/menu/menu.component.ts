import { Component, OnInit } from '@angular/core';
import { Dish} from "../PojoClasses/Dish";
import { DISHES } from "../PojoClasses/Dishes";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent implements OnInit {

  VidePropertyList: Dish[] = DISHES;

  selectedDish: Dish;

  constructor() { }

  ngOnInit() {

  }

  onSelect(dish:Dish){
    this.selectedDish = dish;
  }

}
